class readystock extends kue{
    // inisiasi variabel jumlah dan berat
   private double Jumlah;
   private double Berat;
   
    // konstruktor nama, jumlah, dan harga
   public readystock(String name,double price, int jumlah){
    super(name, price); 
    this.Jumlah = jumlah;
   }

    //mendapatkan jumlah     
   @Override public double Jumlah(){
    return this.Jumlah;
   }

   @Override public double Berat(){
    return Berat;
   }

    //menghitung harga kue dengan mengambil data dari method getPrice    
   public double hitungHarga(){
    return getPrice()*Jumlah()*2;
   }
}