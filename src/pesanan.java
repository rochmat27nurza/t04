class pesanan extends kue{
    private double Berat;
    private double Jumlah;

    // konstruktor untuk nama, harga, berat
    public pesanan(String name, double price, double berat){
        super(name, price); //superclass untuk nama dan harga yang berasal dari kelas parent
        this.Berat = berat;
    }

    // untuk mendapatkan nilai berat kue
    @Override public double Berat(){
        return this.Berat;
    }

    @Override public double Jumlah(){
        return Jumlah; 
    }
  
    @Override public double hitungHarga(){
        return getPrice()*Berat();
    }
}